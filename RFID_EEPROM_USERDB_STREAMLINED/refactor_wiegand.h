#ifndef refactor_wiegand_h
#define refactor_wiegand_h
#include <Arduino.h>
#include <Wiegand.h>

#define PIN_D0 2
#define PIN_D1 3
void stateChanged(bool plugged, const char* message);
void receivedData(uint8_t* data, uint8_t bits, const char* message);
void receivedDataError(Wiegand::DataError error, uint8_t* rawData, uint8_t rawBits, const char* message);
void pinStateChanged();
void do_wiegand_setup();
void do_wiegand_flush();
#endif //refactor_wiegand_h

