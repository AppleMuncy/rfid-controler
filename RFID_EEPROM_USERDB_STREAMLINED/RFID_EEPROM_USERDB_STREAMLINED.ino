
/*
  Open Source RFID Access Controller

  4/3/2011 v1.32
  Last build test with Arduino v00.21
  Arclight - arclight@23.org
  Danozano - danozano@gmail.com

  Notice: This is free software and is probably buggy. Use it at
  at your own peril.  Use of this software may result in your
  doors being left open, your stuff going missing, or buggery by
  high seas pirates. No warranties are expressed on implied.
  You are warned.

  For latest downloads, including Eagle CAD files for the hardware, check out
  http://code.google.com/p/open-access-control/downloads/list

  Latest update moves strings to PROGMEM to free up memory and adds a
  console password feature.


  This program interfaces the Arduino to RFID, PIN pad and all
  other input devices using the Wiegand-26 Communications
  Protocol. It is recommended that the keypad inputs be
  opto-isolated in case a malicious user shorts out the
  input device.
  Outputs go to a Darlington relay driver array for door hardware/etc control.
  Analog inputs are used for alarm sensor monitoring.  These should be
  isolated as well, since many sensors use +12V. Note that resistors of
  different values can be used on each zone to detect shorting of the sensor
  or wiring.

  Version 1.00+ of the hardware implements these features and uses the following pin
  assignments on a standard Arduino Duemilanova or Uno:

  Relay outpus on digital pins 6,7,8,9
  DS1307 Real Time Clock (I2C):A4 (SDA), A5 (SCL)m 70343B 1$99bead00
  Analog pins (for alarm):A0,A1,A2,A3
  Reader 1: pins 2,3
  Reader 2: pins 4,5
  Ethernet: pins 10,11,12,13 (Not connected to the board, reserved for the Ethernet shield)

  Quickstart tips:
  Set the PASSWORD
  Define the static user list by swiping a tag and copying the value received into the #define values shown below
  Compile and upload the code, then log in via serial console at 57600,8,N,1
  9/10/2012 SDC
  ReRe-do of the EEPROM stuff, yo.
  some notes:
  considering string of FF's a blank
  but you're not guaranteed a clear EEPROM yr first time out.
  so you actually have to manually clear it B4 adding users
  9/16/2012 SDC
  x and y (delete/upsert) mostly work.
  weird behavior w/ tag 'ffffff', tho!
  suddenly can't add anything
  (fixed, reset byteval)
  note, restrict tag no to < ffffff (& 0xFFFFFF)
  12/23/2012 SDC
  Let's wrap shit up. Includes: one-shot command and auth.
  Really shitty (tag w/ pwd) at this point, but move to a hashing.
  new add: append $password at end of your command to bypass restricted mode

  1/16/2015 SDC

  Add 'exit' sensor
  Ensure pass lockout.
  Used online pretty printer to fix the craptacular formatting: http://prettyprinter.de/index.php
  maybe try this too http://codebeautify.org/

  11/20/2021 Apple

  I also ripped out all the alarm stuff.
  We are not using it.

  I will be ripping out great chunks Till I get something to compile.

  Will try to leave EEPROM_UserDB inplace


*/

//#include <Wire.h>         // Needed for I2C Connection to the DS1307 date/time chip
//#include <EEPROM.h>       // Needed for saving to non-voilatile memory on the Arduino.

// I'm planning to use the F macro instead of this:
// F macro seems to be working fine.
// so I'm ripping out the pgmspace stuff.
//#include <avr/pgmspace.h> // Allows data to be stored in FLASH instead of RAM

// https://github.com/paulo-raca/YetAnotherArduinoWiegandLibrary
#include "refactor_wiegand.h"

#include "EEPROM_UserDB.h"
#include "messages.h"

//#include <DS1307.h>       // DS1307 RTC Clock/Date/Time chip library
//#include <WIEGAND26.h>    // Wiegand 26 reader format libary
//#include <PCATTACH.h>     // Pcint.h implementation, allows for >2 software interupts.
/* Static user List - Implemented as an array for testing and access override
*/
#define DEBUG 2                         // Set to 2 for display of raw tag numbers in log files, 1 for only denied, 0 for never.
#define sdc     0xFFFFFF                  // Name and badge number in HEX. We are not using checksums or site ID, just the whole
#define dosman  0xFFFFFF
#define apple   0xFFFFFF
// output string from the reader.
const long  superUserList[] = {
  dosman, sdc
}
;
// Super user table (cannot be changed by software)
#define DOORDELAY 5000                  // How long to open door lock once access is granted. (2500 = 2.5s)
#define SENSORTHRESHOLD 100             // Analog sensor change that will trigger an alarm (0..255)
#define DOORPIN1 relayPins[0]           // Define the pin for electrified door 1 hardware
#define DOORPIN2 relayPins[2]           // Define the pin for electrified door 2 hardware
#define EEPROM_ALARMZONES 20            // Starting address to store "normal" analog values for alarm zone sensor reads.
#define PRIV_TIMEOUT 60000 // you get a minute, then lockout
long privTimer = 0;
//byte reader1Pins[]={
//	2,3
//};

//#define PIN_D0 2
//#define PIN_D1 3


// Reader 1 connected to pins 4,5
//byte reader2Pins[]= {
//	4,5
//}
//;
// Reader2 connected to pins 6,7
//const byte analogsensorPins[] = {
//	0,1,2,3
//};
// Alarm Sensors connected to other analog pins
const byte relayPins[] = {
  6, 7
  //  8,9
}
;
// Relay output pins
bool door1Locked = true;
// Keeps track of whether the doors are supposed to be locked right now
//bool door2Locked=true;
unsigned long door1locktimer = 0;
// Keep track of when door is supposed to be relocked
//unsigned long door2locktimer=0;
boolean doorClosed = false;
unsigned long consolefailTimer = 0;
// Console password timer for failed logins
byte consoleFail = 0;
#define numUsers (sizeof(superUserList)/sizeof(long))                  //User access array size (used in later loops/etc)
#define NUMDOORS (sizeof(doorPin)/sizeof(byte))
//#define numAlarmPins (sizeof(analogsensorPins)/sizeof(byte))
// going this way son
;
char PASSWORD[] = "99bead00";
//Other global variables
//byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
// Global RTC clock variables. Can be set using DS1307.getDate function.

//boolean sensor[4]={
//	false
//};
// Keep track of tripped sensors, do not log again until reset.
//unsigned long sensorDelay[2]={
//	0
//};
// Same as above, but sets a timer for 2 of them. Useful for logging
// motion detector hits for "occupancy check" functions.
// Enable up to 3 door access readers.
volatile long reader1 = 0;
volatile int  reader1Count = 0;

//volatile long reader2 = 0;
//volatile int  reader2Count = 0;
int userMask1 = 0;
//int userMask2=0;
// Serial terminal buffer (needs to be global)
char inString[40] = {
  0
}
;
// Size of command buffer (<=128 for Arduino)
byte inCount = 0;
boolean privmodeEnabled = false;
// Switch for enabling "priveleged" commands
/* Create an instance of the various C++ libraries we are using.
*/
//DS1307 ds1307;
// RTC Instance
//WIEGAND26 wiegand26;
// Wiegand26 (RFID reader serial protocol) library
//PCATTACH pcattach;
// Software interrupt library
EEPROM_UserDB UserDB;

// The object that handles the wiegand protocol
// Wiegand wiegand;

void doorLock(int input);
void  readCommand();

void setup() {
  // Runs once at Arduino boot-up
  Serial.begin(57600);
  // Set up Serial output at 8,N,1,57600bps

  Serial.println(F("Starting up"));

  //Initialize output relays
  for (byte i = 0; i < 2; i++) {
    pinMode(relayPins[i], OUTPUT);
    digitalWrite(relayPins[i], LOW);
    // Sets the relay outputs to LOW (relays off)

  }

  do_wiegand_setup();



  Serial.println(F("Finished  setup"));

  //logReboot();

}

void loop()                                     // Main branch, runs over and over again
{
  //  Serial.println("Looping");
  //  delay(1000);
  // Every few milliseconds, check for pending messages on the wiegand reader
  // This executes with interruptions disabled, since the Wiegand library is not thread-safe

  do_wiegand_flush();

//Sleep a little -- this doesn't have to run very often.
  delay(20);

  readCommand();

  // Check for commands entered at serial console
  if (privmodeEnabled && ((millis() - privTimer) > PRIV_TIMEOUT)) {
    privmodeEnabled = false;
    privsenabledMessage();
  }
  /* Check if doors are supposed to be locked and lock/unlock them
    if needed. Uses global variables that can be set in other functions.
  */

  if (((millis() - door1locktimer) >= DOORDELAY) && (door1locktimer != 0))
  {
    if (door1Locked == true) {
      doorLock(1);
      door1locktimer = 0;
    }
    else {
      doorUnlock(1);
      door1locktimer = 0;
      ;
    }
  }
  //	if(((millis() - door2locktimer) >= DOORDELAY) && (door2locktimer !=0))
  //	{
  //		if(door2Locked==true) {
  //			doorLock(2);
  //			door2locktimer=0;
  //		}
  //		else {
  //			doorUnlock(2);
  //			door2locktimer=0;
  //		}
  //	}

  /*  Set optional "failsafe" time to lock up every night.
  */
  /*
    //	ds1307.getDateDs1307(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  	// Get the current date/time
  	if(hour==23 && minute==59 && door1Locked==false){                                        Serial.println();
  		doorLock(1);
  		door1Locked==true;
  		Serial.println(F("Door 1 locked for 2359 bed time."));
  	}
  	// Notes: RFID polling is interrupt driven, just test for the reader1Count value to climb to the bit length of the key
  	// change reader1Count & reader1 et. al. to arrays for loop handling of multiple reader output events
  	// later change them for interrupt handling as well!
  	// currently hardcoded for a single reader unit
  	 This code checks a reader with a 26-bit keycard input. Use the second routine for readers with keypads.
  	 A 5-second window for commands is opened after each successful key access read.
  */
  /*
    if(reader1Count >= 26){
  	//  When tag presented to reader1 (No keypad on this reader)
  	logTagPresent(reader1,1);
  	//  write log entry to serial port
  	 Check a user's security level and take action as needed. The
  	  usermask is a variable from 0..255. By default, 0 and 255 are for
  	  locked out users or uninitialized records.
  	  Modify these for each door as needed.

  	userMask1 = UserDB.checkUser(reader1);
  	if(userMask1>=0) {
  		switch(userMask1) {
  			case 0:                                      // No outside privs, do not log denied.
  			{
  				// authenticate only.
  				logAccessGranted(reader1, 1);
  				break;
  			}
  			case 255:                                              // Locked out user
  			{
  				Serial.print(F("User "));
  				Serial.print(userMask1,DEC);
  				Serial.println(F(" locked out."));
  				break;
  			}
  			default:
  			{
  				logAccessGranted(reader1, 1);
  				// Log and unlock door 1
  				door1locktimer=millis();
  				doorUnlock(1);
  				// Unlock the door.
  				break;
  			}
  		}
  	}
  	else
  	{
  		if(checkSuperuser(reader1) >= 0) {
  			// Check if a superuser, grant access.
  			logAccessGranted(reader1, 1);
  			// Log and unlock door 1
  			door1locktimer=millis();
  			doorUnlock(1);
  			// Unlock the door.
  		}
  		else {
  			logAccessDenied(reader1,1);
  		}
  	}
    //		wiegand26.initReaderOne();
  	// Reset for next tag scan
    }
    if(reader2Count >= 26){
  	// Tag presented to reader 2
  	logTagPresent(reader2,2);
  	// Write log entry to serial port
  	// CHECK TAG IN OUR LIST OF USERS. -1 = no match
  	userMask2=UserDB.checkUser(reader2);
  	if(userMask2>=0){
  		switch(userMask2) {
  			case 0:                         // No outside privs, do not log denied.
  			{
  				// authenticate and log only.
  				logAccessGranted(reader2, 2);
  				break;
  			}
  			case 10:                         // Authenticating immediately locks up and arms alarm
  			{
  				logAccessGranted(reader2, 2);
  				break;
  			}
  			case 255:                                               // Locked out
  			{
  				Serial.print(F("User "));
  				Serial.print(userMask2,DEC);
  				Serial.println(F(" locked out."));
  				break;
  			}
  			default:
  			{
  				logAccessGranted(reader2, 2);
  				// Log and unlock door 2
  				door2locktimer=millis();
  				doorUnlock(2);
  				break;
  			}
  		}
  	}
  	else
  	{
  		if(checkSuperuser(reader2) >= 0) {
  			// Check if a superuser, grant access.
  			logAccessGranted(reader2, 2);
  			// Log and unlock door 2
  			door1locktimer=millis();
  			doorUnlock(1);
  			// Unlock the door.
  		}
  		else{
  			logAccessDenied(reader2,2);
  		}
  	}
    //		wiegand26.initReaderTwo();
  	//  Reset for next tag scan
    }

    // Log all motion detector activations. Useful for "occupancy detection"
    if(pollAlarm(0) == 1 ){
  	// If this zone is tripped, log the action only
  	//  if(sensor[0]==false)
  	if((millis() - sensorDelay[0]) >=7500) {
  		logalarmSensor(0);
  		sensorDelay[0]=millis();
  		sensor[0]=true;
  	}
  	// Set value to not log this again for 7.5s
    }
    if(pollAlarm(1) == 1 ){
  	// If this zone is tripped, log the action only
  	//   if(sensor[1]==false)
  	if((millis() - sensorDelay[1]) >=7500) {
  		logalarmSensor(1);
  		sensorDelay[1]=millis();
  		sensor[1]=true;
  		// Set value to not log this again for 7.5s
  	}
    }

    if (pollAlarm(3) == 1 && door1locktimer == 0) {
          Serial.println(F("Exit button pressed."));
          doorUnlock(1);
    door1locktimer=millis();
        }
  */
}
// End of loop()


/* Access System Functions - Modify these as needed for your application.
  These function control lock/unlock and user lookup.
*/
int checkSuperuser(long input) {
  // Check to see if user is in the user list. If yes, return their index value.
  int found = -1;
  for (int i = 0; i <= numUsers; i++) {
    if (input == superUserList[i]) {
      //			logDate();
      Serial.print("Superuser ");
      Serial.print(i, DEC);
      Serial.println(" found.");
      found = i;
      return found;
    }
    ;
  }
  return found;
  //If no, return -1
}

void doorUnlock(int input) {
  //Send an unlock signal to the door and flash the Door LED
  byte dp = 1;
  if (input == 1) {
    dp = DOORPIN1;
  }
  else(dp = DOORPIN2);
  digitalWrite(dp, HIGH);
  Serial.print(F("Door "));
  Serial.print(input, DEC);
  Serial.println(F(" unlocked"));
}


void doorLock(int input) {
  //Send an unlock signal to the door and flash the Door LED
  byte dp = 1;
  if (input == 1) {
    dp = DOORPIN1;
  }
  //	else(dp=DOORPIN2);
  digitalWrite(dp, LOW);
  Serial.print(F("Door "));
  Serial.print(input, DEC);
  Serial.println(F(" locked"));
}

void lockall() {
  //Lock down all doors. Can also be run periodically to safeguard system.
  digitalWrite(DOORPIN1, LOW);
  //	digitalWrite(DOORPIN2,LOW);
  door1Locked = true;
  //	door2Locked=true;
  doorslockedMessage();
}

/*
  Logging Functions - Modify these as needed for your application.
  Logging may be serial to USB or via Ethernet (to be added later)

*/




void logReboot() {
  //	logDate();
  rebootMessage;
}

void logTagPresent (long user, byte reader) {
  //	logDate();
  Serial.print(F("User "));
  if (DEBUG == 2) {
    Serial.print(user, HEX);                                        Serial.println();
  }
  Serial.print(F(" presented tag at reader "));
  Serial.println(reader, DEC);
}

void logAccessGranted(long user, byte reader) {
  //Log Access events
  //	logDate();
  Serial.print(F("User "));
  if (DEBUG == 2) {
    Serial.print(user, HEX);
  }
  Serial.print(F(" granted access at reader "));
  Serial.println(reader, DEC);
}

void logAccessDenied(long user, byte reader) {
  //Log Access denied events
  //	logDate();
  Serial.print(F("User "));
  if (DEBUG == 1) {
    Serial.print(user, HEX);
  }
  Serial.print(F(" denied access at reader "));
  Serial.println(reader, DEC);
}
void logalarmSensor(byte zone) {
  //	logDate();
  Serial.print(F("Zone "));
  Serial.print(zone, DEC);
  Serial.println(F(" sensor activated"));
}

void logunLock(long user, byte door) {
  //logDate();
  Serial.print(F("User "));
  Serial.print(user, DEC);
  Serial.print(F(" unlocked door "));
  Serial.println(door, DEC);
}

void logprivFail() {
  privsdeniedMessage();
}

/* Displays a serial terminal menu system for
  user management and other tasks
*/
void readCommand() {
  boolean requestValidated = false;
  // valid for current request only
  byte stringSize = (sizeof(inString) / sizeof(char));
  char cmdString[4][9];
  // Size of commands (4=number of items to parse, 10 = max length of each)
  char password[10];
  // max (ieeee)
  byte j = 0;
  // Counters
  byte k = 0;
  char cmd = 0;
  char ch;
  password[0] = 0;
  /*
    SDC remarks. Slightly hacky 'signing'
    command$password
    will check the password and kick off priv mode if good.
    gives the 'too bad' msg if not.
  */
  if (Serial.available()) {

    // Check if user entered a command this round
    ch = Serial.read();
    if ( ch == '\r' || inCount >= stringSize - 1)  {
      // Check if this is the terminating carriage return
      inString[inCount] = 0;
      inCount = 0;

    }
    else {
      (inString[inCount++] = ch);
    }
    Serial.print(ch);
    // Turns echo on or off
    if (inCount == 0) {
      // done readin
      cmd = inString[0];
      for (byte i = 0;
           i < stringSize && password[0] == 0;
           i++) {
        cmdString[j][k] = inString[i];
        if (k < 9) k++;
        else break;
        if (inString[i] == ' ') // Check for space and if true, terminate string and move to next string.
        {
          cmdString[j][k - 1] = 0;
          if (j < 3)j++;
          else break;
          k = 0;
        }
        else if (inString[i] == '$') {
          // terminate and move on(?)
          cmdString[j][k - 1] = 0;
          // this 'backing up - not good son'
          j++;
          k = 1;
          while (inString[i + k] != 'r') {
            password[k - 1] = inString[i + k];
            if (k < 10) k++;
            else break;
          }
          password[k] = 0;
          break;
        }
        // password is always last - note, try a more unified approach.
      }
      // password always seems to wipe cmd
      // cmd = cmdString[0][0];
      // weird. just assign cmd at the beginning. then all is ok. This is fucked                                        Serial.println();
      if (password[0] != 0) {
        if ((consoleFail >= 5) && (millis() - consolefailTimer < 300000)) // Do not allow priv mode if more than 5 failed logins in 5 minute
        {
          privsAttemptsMessage();
        }
        else if (strcmp(password, PASSWORD) == 0) {
          consoleFail = 0;
          requestValidated = true;
        }
        else {
          if (consoleFail == 0) {
            // Set the timeout for failed logins
            consolefailTimer = millis();
          }
          consoleFail++;
          // Increment the login failure counter
        }
        privsdisabledMessage();//
      }
      //			logDate();
      // note - privmodeEnabled is a per request in this model.
      switch (cmd) {
        case 'e': {
            // Enable "privileged" commands at console
            if ((consoleFail >= 5) && (millis() - consolefailTimer < 300000)) // Do not allow priv mode if more than 5 failed logins in 5 minute
            {
              privsAttemptsMessage();
            }
            // compare the substring
            if (strcmp(cmdString[1], PASSWORD) == 0)
            {
              consoleFail = 0;
              privsenabledMessage();
              privmodeEnabled = true;
              privTimer = millis();
            }
            else {
              privsdisabledMessage();
              privmodeEnabled = false;
              if (consoleFail == 0) {
                // Set the timeout for failed logins
                consolefailTimer = millis();
              }
              consoleFail++;
              // Increment the login failure counter -- only drops if success!
            }
            break;
          }
        case 'a': {
            // List whole user database
            if (true) {
              //					if(privmodeEnabled==true || requestValidated == true) {
              //						logDate();
              Serial.println("");
              Serial.print("UserNum:");
              Serial.print("t");
              Serial.print("Usermask:");
              Serial.print("t");
              Serial.println("TagNum:");
              UserDB.dumpUsers();
            }
            else {
              logprivFail();
            }
            break;
          }




        case 's': {
            // List user
            if (privmodeEnabled == true || requestValidated == true) {
              UserDB.getUserMask(strtoul(cmdString[1], NULL, 16));
            }
            else {
              logprivFail();
            }
            break;
          }
        case 'd': {
            // Display current time
            //					logDate();
            Serial.println("now");
            break;
          }

        case 'u': {
            if (privmodeEnabled == true || requestValidated == true) {
              doorUnlock(1);
              //						doorUnlock(2);
              door1Locked = false;
              //						door2Locked=false;
            }
            else {
              logprivFail();
            }
            break;
          }
        case 'l': {
            // Lock all doors
            lockall();
            //  chirpAlarm(1);
            break;
          }
        case '3': {
            // Train alarm sensors
            //					if(privmodeEnabled==true || requestValidated == true) {
            //						trainAlarm();
            //					}
            //					else{
            logprivFail();
            //					}
            break;
          }
        case '9': {
            // Show site status
            statusMessage3();
            //					Serial.println(pollAlarm(3),DEC);
            statusMessage4();
            //					Serial.println(pollAlarm(2),DEC);
            statusMessage5();
            Serial.println(door1Locked);
            statusMessage6();
            //					Serial.println(door2Locked);
            break;
          }
        case 'o': {
            if (privmodeEnabled == true || requestValidated == true) {
              if (atoi(cmdString[1]) == 1) {
                doorUnlock(1);
                // Open the door specified
                door1locktimer = millis();
                break;
              }
              if (atoi(cmdString[1]) == 2) {
                //							doorUnlock(2);                                        Serial.println();
                //							door2locktimer=millis();
                break;
              }
              Serial.print(F("Invalid door number!"));
            }
            else {
              logprivFail();
            }
            break;
          }
        case 'm': {
            // add or update
            if (privmodeEnabled == true || requestValidated == true) {
              UserDB.upsertUser(atoi(cmdString[2]), strtoul(cmdString[1], NULL, 16));
            }
            else {
              logprivFail();
            }                                        Serial.println();
            break;
          }
        case 'z': {
            // 'zap' it
            if (privmodeEnabled == true || requestValidated == true) {
              UserDB.clearUsers();
            }
            else {
              logprivFail();
            }
            break;
          }
        case 'r': {
            if (privmodeEnabled == true || requestValidated == true) {
              UserDB.deleteUser(strtoul(cmdString[1], NULL, 16));
            }
            else {
              logprivFail();
            }
            break;
          }
        case '?': {
            // Display help menu
            consolehelpMessage1();
            Serial.println();
            consolehelpMessage2();
            Serial.println();
            consolehelpMessage3();
            Serial.println();
            consolehelpMessage4();
            Serial.println();
            consolehelpMessage5();
            Serial.println();
            consolehelpMessage6();
            break;
          }
        default:
          consoledefaultMessage();
          break;
      }
      // end switch
    }
    // End of 'if' for reading is done
  }
  // End of 'if' for serial reading.
}
// End of function
/* Wrapper functions for interrupt attachment;

  Could be cleaned up in library?
*/
void callReader1Zero() {
  //	wiegand26.reader1Zero();
}
void callReader1One() {
  //	wiegand26.reader1One();
}
void callReader2Zero() {
  //	wiegand26.reader2Zero();
}
void callReader2One() {
  //	wiegand26.reader2One();
}
void callReader3Zero() {
  //	wiegand26.reader3Zero();
}
void callReader3One() {
  //	wiegand26.reader3One();
}





