// console messages that will mostly be placed in flash memory with the F macro
//
#ifndef messages_h
#define messages_h
//
/* Set up some strings that will live in flash instead of memory. This saves our precious 2k of
* RAM for something else.
*/
void rebootMessage(){
	Serial.print (F("Access Control System rebooted."));
}

void doorChimeMessage(){
	Serial.print(F("Front Door opened."));
}

void doorslockedMessage(){
	Serial.print(F("All Doors relocked"));
}

void alarmtrainMessage(){
  Serial.print(F(	"Alarm Training performed."));
}

void privsdeniedMessage(){
	Serial.print(F("Access Denied. Priveleged mode is not enabled."));
}

void privsenabledMessage(){
	Serial.print(F("Priveleged mode enabled."));
}

void privsdisabledMessage(){
	Serial.print(F("Priveleged mode disabled."));
}

void privsAttemptsMessage(){
	Serial.print(F("Too many failed attempts. Try again later."));
}

void consolehelpMessage1(){
  Serial.print(F(	"Valid commands are:"));
}
;

void consolehelpMessage2(){
	Serial.print(F("(d)ate, (s)show user <tagNumber>, (m)odify user <tagnumber> <usermask>"));
}

void consolehelpMessage3(){
	Serial.print(F("(a)ll user dump, (r) remove user <tagnumber>, (o)open door <num>"));
}

void consolehelpMessage4(){
	Serial.print(F("(z)ap all users, (u)nlock all doors,(l)lock all doors"));
}

void consolehelpMessage5(){
	Serial.print(F("(3)train_sensors (9)show_status"));
;
}

void consolehelpMessage6(){
	Serial.print(F("(e)nable <password> - enable or disable priveleged mode"));
}

void consoledefaultMessage(){
	Serial.print(F("Invalid command. Press '?' for help."));
}


void statusMessage3(){
	Serial.print(F("Front door open state (0=clsed):"));
}

void statusMessage4(){
	Serial.print(F("Roll up door open state (0=closed):"));
}

void statusMessage5(){
	Serial.print(F("Door 1 unlocked state(1=locked):"));
}

void statusMessage6(){
	Serial.print(F("Door 2 unlocked state(1=locked):"));
}


#endif  //messages_h
